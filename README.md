**Node.js v6.1.0
MongoDB v2.4.9**

***Установка и запуск:*** 

cd (/pathToProject/)

**Установка глобального бабеля и пакетов:**

npm install babel@5 -g

npm install

**Запуск серверной части:**

mongod

babel-node server/app.js

**
Запуск webpack-devserver:**

npm run webpack-devserver