import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const MarkSchema = new Schema({
  lat: { type: Number, required: true },
  lng: { type: Number, required: true},
  description: { type: String },
  obj: [
    {
      name: {type: String}
    }
  ]
});

const Mark = mongoose.model('Mark', MarkSchema);