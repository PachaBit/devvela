import mongoose from 'mongoose';

import '../models/Mark';

import config from '../../etc/config.json';

const Mark = mongoose.model('Mark');

export function setUpConnection() {
  mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.name}`);
}

export function listMarks() {
  return Mark.find();
}

export function createMark(data) {
  const mark = new Mark({
    lat: data.lat,
    lng: data.lng
  });

  return mark.save();
}

export function deleteMark(id) {
  return Mark.findById(id).remove();
}

export function changeDescription(id, data) {
  return Mark.findById(id).update(data);
}

export function createMarkObject(id, data) {
  return Mark.findByIdAndUpdate(id, {$push: {"obj": data.markObject}}, {safe: true, upsert: true, new : true});
}

