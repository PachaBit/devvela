import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { serverPort } from '../etc/config.json';
import * as db from './utils/DataBaseUtils.js';

db.setUpConnection();

const app = express();

app.use( bodyParser.json() );

app.use ( cors({ origin: '*'}) );

app.get('/marks', (req, res) => {
  db.listMarks().then(data => res.send(data));
});

app.post('/marks', (req, res) => {
  db.createMark(req.body).then(data => res.send(data));
});

app.put('/marks/:id', (req, res) => {
  db.changeDescription(req.params.id, req.body).then(data => res.send(data));
});

app.delete('/marks/:id', (req, res) => {
  db.deleteMark(req.params.id).then(data => res.send(data));
});

app.post('/marks/:id', (req, res) => {
  db.createMarkObject(req.params.id, req.body).then(data => res.send(data));
});

const server = app.listen(8080, () => {
  console.log(`Server is running on port ${serverPort}`);
});
