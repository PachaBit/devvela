import alt from '../alt';

import api from '../api/index.js';

class MarksActions {
  fetchMarks() {
    return dispatch => {
      api.fetchMarks()
        .then(response => {
          dispatch(response.data);
        })
        .catch(error => console.error(error));
    }
  }

  createMark(data) {
    return dispatch => {
      api.createMark(data)
        .then(response => {
          dispatch(response.data);
        })
        .catch(error => console.error(error));
    }
  }

  changeDescription(markId, data) {
    return dispatch => {
      api.changeDescription(markId, data)
        .then(() => {
          dispatch({
            id: markId,
            data: data
          });
        })
        .catch(error => console.error(error));
    }
  }

  deleteMark(markId) {
    return dispatch => {
      api.deleteMark(markId)
        .then(() => {
          dispatch(markId);
        })
        .catch(error => console.error(error));
    }
  }

  createMarkObject(data) {
    return dispatch => {
      api.createMarkObject(data)
        .then(response => {
          dispatch(response.data)
        })
        .catch(error => console.error(error));
    }
  }
}

module.exports = alt.createActions(MarksActions);