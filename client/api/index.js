import axios from 'axios';

import { apiPrefix } from '../../etc/config.json';

export default {
  fetchMarks() {
    return axios.get(`${apiPrefix}/marks`);
  },

  createMark(data) {
    return axios.post(`${apiPrefix}/marks`, data);
  },

  changeDescription(id, data) {
    return axios.put(`${apiPrefix}/marks/${id}`, data);
  },

  deleteMark(id) {
    return axios.delete(`${apiPrefix}/marks/${id}`);
  },

  createMarkObject(data) {
    return axios.post(`${apiPrefix}/marks/${data.markId}`, data);
  }
}

