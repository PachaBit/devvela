import alt from '../alt';

import api from "../api/index.js";

import MarksActions from '../actions/MarksActions.js';

class MarksStore {
  constructor() {
    this.state = {
      marks: []
    };

    this.bindAction(MarksActions.fetchMarks, this.handleFetchMarks);
    this.bindAction(MarksActions.createMark, this.handleCreateMark);
    this.bindAction(MarksActions.changeDescription, this.handleChangeDescription);
    this.bindAction(MarksActions.deleteMark, this.handleDeleteMark);
    this.bindAction(MarksActions.createMarkObject, this.handleCreateMarkObject);
  }

  handleFetchMarks(marks) {
    this.setState({marks});
    console.log(this.state);
  }

  handleCreateMark(mark) {
    this.setState({
      marks: [
        ...this.state.marks,
        mark
      ]
    });
  }

  handleChangeDescription(changedMark) {
    this.setState({
      marks: this.state.marks.map( mark => mark._id === changedMark.id ? Object.assign({}, mark, changedMark.data) : mark )
    });
  }

  handleDeleteMark(markId) {
    this.setState({
      marks: this.state.marks.filter(mark => mark._id !== markId)
    });
  }

  handleCreateMarkObject() {
    MarksActions.fetchMarks();
  }
}

module.exports = alt.createStore(MarksStore, 'MarksStore');