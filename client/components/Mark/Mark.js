import React from 'react';

import './Mark.less';

class Mark extends React.Component {

  constructor() {
    super();
    this.state = {
      description: ""
    }
  }

  componentDidMount() {
    this.getDescription();
  }

  getDescription() {
    this.setState({
      description: this.props.description
    });
  }


  render() {
    function onClick() {
      console.log('mark Clicked');
    }

    return (
      <div className="mark" onClick={onClick}>

        <div className="mark__description">
          {this.props.description ? this.props.description : "Описание отсутствует"}
        </div>
      </div>
    )
  }
}

export default Mark;