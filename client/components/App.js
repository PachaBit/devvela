import React from 'react';

import './App.less';

import Map from './Map/Map.js';
import MarkList from './MarkList/MarkList.js';

class App extends React.Component {
  render() {
    return(
      <div className="app">
        <Map />
        <MarkList />
      </div>
    )
  }
}

export default App;