import React from 'react';

class MarkObjectsList extends React.Component {
  render() {
    return(
      <div className="mark-objects-item">
        {this.props.objectName}
      </div>
    )
  }
}

export default MarkObjectsList;