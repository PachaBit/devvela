import React from 'react';

import './MarkListItem.less';

import Description from '../Description/Description.js';
import MarkObjects from '../MarkObjects/MarkObjects.js';

import MarksActions from '../../actions/MarksActions.js';

class MarkListItem extends React.Component {

  deleteMark = () => {
    MarksActions.deleteMark(this.props.id);
  };


  render() {
    return(
      <div className="mark-list-item">
        <div className="mark-list-item__img"></div>
        <div className="mark-list-item__info">
          <Description description={this.props.description} id={this.props.id} />
          <MarkObjects id={this.props.id} markObj={this.props.markObj} />
        </div>
        <div className="delete-btn" onClick={this.deleteMark}></div>
      </div>
    )
  }
}

export default MarkListItem;