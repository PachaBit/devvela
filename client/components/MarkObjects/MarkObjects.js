import React from 'react';

import './MarkObjects.less';

import Input from 'muicss/lib/react/input';

import MarksActions from '../../actions/MarksActions.js';

import MarkObjectsList from '../MarkObjectsList/MarkObjectsList.js';


class MarkObjects extends React.Component {
  constructor() {
    super();
    this.state = {
      inputValue: '',
    }
  }

  onSubmit = e => {
    if (e.key === 'Enter') {
      MarksActions.createMarkObject({
        markId: this.props.id,
        markObject: {name: this.state.inputValue}
      });
      this.setState({inputValue: ''});
      e.preventDefault();
    }
  };

  render() {
    return(
      <div className="mark-objects">
        {this.props.markObj.map((obj, index) => <MarkObjectsList id={this.props.id} objectName={obj.name} key={index} />)}
        <Input
          placeholder="Введите название объекта"
          value={this.state.inputValue}
          onChange={e => this.setState({inputValue: e.target.value})}
          label="Новый объект"
          onKeyPress={this.onSubmit}
        />
      </div>
    )
  }
}

export default MarkObjects;