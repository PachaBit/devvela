import React from 'react';

import './MarkList.less';

import MarksStore from '../../stores/MarksStore.js';
import MarksActions from '../../actions/MarksActions.js';

import MarkListItem from '../MarkListItem/MarkListItem.js';

class MarkList extends React.Component {

  constructor() {
    super();
    this.state = {
      ...MarksStore.getState()
    }
  }

  componentDidMount() {
    MarksStore.listen(this.onChange);
    MarksActions.fetchMarks();
  }

  onChange = state => this.setState(state);

  render() {
    return(
      <div className="mark-list">
        {this.state.marks.map((mark, index) => <MarkListItem description={mark.lat} id={mark._id} key={index} markObj={mark.obj}/>)}
      </div>
    )
  }
}

export default MarkList;