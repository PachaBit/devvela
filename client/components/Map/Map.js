import React from 'react';

import GoogleMap from 'google-map-react';

import MarksStore from '../../stores/MarksStore.js';
import MarksActions from '../../actions/MarksActions.js';

import Mark from '../Mark/Mark.js';

class Map extends React.Component {

  constructor() {
    super();
    this.state = {
      ...MarksStore.getState()
    }
  }

  componentDidMount() {
    MarksStore.listen(this.onChange);
    MarksActions.fetchMarks();
  }

  onChange = state => this.setState(state);


  createMark = clickCoords => {
    MarksActions.createMark({lat: clickCoords.lat, lng: clickCoords.lng });
  };

  render() {

    return(
      <GoogleMap
        defaultCenter={{lat:50, lng:30}}
        defaultZoom={5}
        bootstrapURLKeys={{key: "AIzaSyDk7-B1O2qHepSkhjMz4Rm2VrjA3l91NQc"}}
        onClick = { this.createMark }
      >
        {this.state.marks.map((mark, index) => <Mark lat={mark.lat} lng={mark.lng} description={mark.description} key={index} />)}
      </GoogleMap>
    )
  }
}

export default Map;