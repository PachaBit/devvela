import React from 'react';

import './Description.less';

import Textarea from 'muicss/lib/react/textarea';
import Button from 'muicss/lib/react/button';

import MarksActions from '../../actions/MarksActions.js';


class Description extends React.Component {

  constructor() {
    super();
    this.state = {
      description: ""
    };
  }

  saveDescription = () => {
    MarksActions.changeDescription(this.props.id, {description: this.state.description});
  };


  render() {
    return(
      <div className="description">
          <Textarea label="Введите описание" floatingLabel={true} onChange={e => this.setState({description: e.target.value})} />
          <Button onClick={this.saveDescription}>Сохранить</Button>
      </div>
    )
  }
}

export default Description;